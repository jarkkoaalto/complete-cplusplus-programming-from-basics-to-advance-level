//============================================================================
// Name        : TableApp.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Table App using while loop in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int table;
	cout<<"Enter a Number: ";
	cin>>table;

	int count = 1;

	while(count <= 10){
		cout<<table<<" * "<<count<< " = "<< table*count <<endl;
		count++;
	}
	return 0;
}


