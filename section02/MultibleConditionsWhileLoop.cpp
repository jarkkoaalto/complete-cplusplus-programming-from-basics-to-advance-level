//============================================================================
// Name        :MultibleConditionsWhileLoop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Multible conditions While loop in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int counter = 0;
	cout <<"How many times you want the loop to run:";
	cin >>counter;

	// bool check = false;
	bool check = true;

	while(counter > 0 && check){
		cout <<counter<<endl;
		counter--;
	}


	return 0;
}


