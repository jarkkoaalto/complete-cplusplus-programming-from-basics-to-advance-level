//============================================================================
// Name        : ForLoop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : For loop in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int userinput;
	cout<<"How mamy time you want loop to run: ";
	cin >> userinput;

	for(int i =1; i <=userinput; i++){
		cout<<i;
	}

	cout<<""<<endl;

	for(int i=0;i<=10;i++){
		cout<<i;
	}

	cout<<""<<endl;

	for(int i=0;i<=10;i=i+2){
			cout<<i;
		}

	cout<<""<<endl;

	for(int i=10;i>=0;i--){
				cout<<i;
			}
	return 0;
}


