//============================================================================
// Name        : Factorial.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Factorial in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int number, factorial;
	cout << "Enter Number: ";
	cin >> number;

	factorial = number;

	for(int i= number-1; i > 0; i--){
		factorial = factorial * i;
	}
	cout << "Factorial is: "<< factorial;
	return 0;
}


