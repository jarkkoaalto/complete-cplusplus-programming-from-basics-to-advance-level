//============================================================================
// Name        :WhileLoop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : While loop in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int counter = 10;
	cout <<"How many times you want the loop to run:";
	cin >>counter;

	while(counter > 0){
		cout <<counter<<endl;
		counter--;
	}


	return 0;
}


