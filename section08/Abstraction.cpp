//============================================================================
// Name        : Abstraction.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Abstraction in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string.h>
using namespace std;

class Shape{
public:
	int width;
	int height;

	void area(){
		cout << "this is area of Shape class which is parent / Base"<<endl;
	}
};

class Rectange: public Shape{

public:
	Rectange(int w, int h){
		width = w;
		height = h;
	}
	void area(){
		cout<<"Area of Rectangle is : "<< width * height <<endl;
	}
};

class Triangle: public Shape{
public:
	Triangle(int w, int h){
		width = w;
		height = h;
	}
	void area(){
		cout<<"Area of Triangle is: "<< (width*height)/2 <<endl;
	}
};

int main() {
	Rectange rectangle(3,5);
	rectangle.area();

	Triangle triangle(5, 10);
	triangle.area();

	return 0;
}
