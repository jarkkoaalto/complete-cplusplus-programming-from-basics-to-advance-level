//============================================================================
// Name        : AccessModifiers.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Access Modifiers in C++, Ansi-style
//============================================================================
#include <iostream>
#include <string.h>
using namespace std;


class Book{
public:
	string name;
	string author;
	string title;
	int price;

	// Functions
	Book(){
		name ="";
		author = "";
		title = "";
		price = 100;
		cout<<"Hey I am Book Constructor"<<endl;
	}

	Book(string nameOfBook, string nameOfAuthor, string titleOfBook, int priceOfBook){
		name = nameOfBook;
		author = nameOfAuthor;
		title = titleOfBook;
		price = priceOfBook;
	}

	void setPrice(int priceValue){
		if(priceValue > 0)
			price = priceValue;
		else
			cout<<"This is not a correct value for price";
	}
	void setNameOfBook(string nameOfBook){
		name = nameOfBook;
	}
	void setAuthor(string authorName){
		author = authorName;
	}
	void setTitle(string titleOfBook){
		title = titleOfBook;
	}
};

class Cookbook: public Book{
public:
	Cookbook(){
		cout<<"Hei I'm constructor of Cookbook class"<<endl;
	}

	void recipe(){
		cout <<"Recipe function"<<endl;
	}
};

int main() {
	Book book;
	Cookbook cookbook;

	cout<<book.title<<endl;
	cout<<cookbook.name<<endl;
	cookbook.recipe();
	return 0;
}

