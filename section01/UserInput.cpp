//============================================================================
// Name        : String.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Strings in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	string name;
	int a, b;

	cout << "Enter Your Name: ";
	cin >> name;
	cout << "Enter value of a: ";
	cin >> a;
	cout << "Enter value of b: ";
	cin >> b;

	cout << "Your Name is: " << name<<endl;
	cout << "Sum of a and b is: "<< a+b <<endl;

	return 0;
}


