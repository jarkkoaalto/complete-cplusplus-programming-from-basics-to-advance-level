//============================================================================
// Name        :first.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : course first program in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "This is my first code" << endl;
	return 0;
}
