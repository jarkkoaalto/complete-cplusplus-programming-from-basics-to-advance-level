//============================================================================
// Name        :VariablesAndNumbers.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Variables And Numbers: int, fload and double in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int sixty = 60;
	int negativeValue = -30;

	float number = 60.30;
	double dnumber = 7.7;

	int sum = sixty + negativeValue;
	int sum1 = sixty + number;

	float sum2 = sixty + number;
	float sum3 = sixty + number + dnumber + 100;

	int substract = sixty - 60;
	int multiply = number * 22;
	int divide = number / 4;

	cout << sum <<endl;
	cout << sum1 <<endl;
	cout << sum2 <<endl;
	cout << sum3 <<endl;
	cout << substract << endl;
	cout << multiply << endl;
	cout << divide << endl;


	return 0;
}
