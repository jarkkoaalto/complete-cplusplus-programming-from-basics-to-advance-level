//============================================================================
// Name        : String.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Strings in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	string  fruit = "apple";
	string longString = "I am very long string example";

	cout << fruit <<endl;
	cout << longString <<endl;

	return 0;
}


