//============================================================================
// Name        : Arrays.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Arrays in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int array[] = {3,5,7};

	int arr[3];
	arr[0] = 99;
	arr[1] = 12;
	arr[2] = 5;

	string name[3];
	name[0] = "Joe";
	name[1] = "David";
	name[2] = "Laura";

	cout<<array[2]<<endl;
	cout<<arr[2]<<endl;
	cout<<name[2]<<endl;
	return 0;
}


