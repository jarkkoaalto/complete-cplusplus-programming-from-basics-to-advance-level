//============================================================================
// Name        : DimensionalArray.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Dimensoinal Array in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int arr2D[3][3] ={
			{4,5,6}, 	// row 1
			{9,8,7},	// row 2
			{4,3,1} 	// row 3
	};
	cout<< arr2D[1][1]<<endl; // 8

	int arr2[3][4] = {
			{1,2,3,4},
			{11,22,33,44},
			{111,222,333,444}
	};
	cout<< arr2[1][2]; // 33
	return 0;
}


