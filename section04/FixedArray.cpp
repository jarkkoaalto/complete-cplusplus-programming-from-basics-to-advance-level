//============================================================================
// Name        : Arrays.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Arrays in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	string name[4];
	name[0] = "Joe";
	name[1] = "David";
	name[2] = "Laura";
	name[3] = "James";
	cout<<name[3]<<endl;
	return 0;
}


