//============================================================================
// Name        :ThirdClass.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Third class in C++, Ansi-style
//============================================================================
#include <iostream>
#include <string.h>

using namespace std;

class Book{
private:
	int price;
public:
	// variables
	string name;
	string author;
	string title;


	Book(){
		name = "";
		author = "";
		title = "";
		price = 100;
	}

	Book(string nameOfBook, string nameOfAuthor, string titleOfBook, int priceOfBook){
		name = nameOfBook;
		author = nameOfAuthor;
		title = titleOfBook;
		price = priceOfBook;
	}
private:
	void setPrice(int priceValue){
		if(priceValue > 0){
			price = priceValue;
		}else{
			cout<<"Thisi is not a correct value of price";
		}
		price = priceValue;
	}
public:
	void setNameOfBook(string nameOfBook){
		name = nameOfBook;
	}

	void setAuthor(string authorName){
		author = authorName;
	}
	void setTitle(string titleOfBook){
		title = titleOfBook;
	}

};

int main() {
	Book book("Learn C++", "Hamza","Learning C++ in detail",200);

	book.setAuthor("Klaus");
	cout<<book.author;

	return 0;
}

