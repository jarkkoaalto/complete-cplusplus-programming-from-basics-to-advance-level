//============================================================================
// Name        :first.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : course first program in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string.h>

using namespace std;

class Book{

public:
	// variables
	string name;
	string author;
	string title;
	int price;

	Book(){
		name = "";
		author = "";
		title = "";
		price = 100;
	}

	void setPrice(int priceValue){
		if(priceValue > 0){
			price = priceValue;
		}else{
			cout<<"Thisi is not a correct value of price";
		}
		price = priceValue;
	}

	void setNameOfBook(string nameOfBook){
		name = nameOfBook;
	}

	void setAuthor(string authorName){
		author = authorName;
	}
	void setTitle(string titleOfBook){
		title = titleOfBook;
	}

};

int main() {
	Book book;
		book.setPrice(300);
		book.setNameOfBook("Jason Bourn");
		book.setAuthor("Robert Ludlum");
		book.setTitle("Medusan perinto");
	Book book2;
		book2.setPrice(200);
		book2.setAuthor("David Eddings");

	cout <<book2.price<<endl;
	cout <<book2.author<<endl;
	cout <<"******   **********"<<endl;
	cout <<book.name<<endl;
	cout <<book.price<<endl;
	cout <<book.author<<endl;
	cout <<book.title<<endl;
	return 0;
}

