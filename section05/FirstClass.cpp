//============================================================================
// Name        :first.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : course first program in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string.h>

using namespace std;

class Book{
public:
	int price;
	int row;

	Book(){
		price = 10;
	}
	void setPrice(){
		price = 500;
	}

	void setShelve(){
		row = 3;
	}
};

int main() {

	Book book;
	book.setPrice();
	book.setShelve();
	cout << book.price<<endl;
	cout << book.row<<endl;
	return 0;
}

