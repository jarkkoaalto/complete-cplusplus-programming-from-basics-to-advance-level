//============================================================================
// Name        : IfElse.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : If Else in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int a = 4;
	int b = 8;
	int c = 16;

	if (a < b && a < c){
		cout << "A is less than B and C";
	}

	return 0;
}


