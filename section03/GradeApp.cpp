//============================================================================
// Name        : GradeAPP.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Grade application in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int marks;
	cout << "Enter Your Marks: ";
	cin >> marks;
	if(marks >= 80){
		cout <<"Your grade is A"<<endl;
	}
	else if(marks >= 70 && marks < 80){
		cout <<"Your grade is B"<<endl;
	}
	else if(marks >= 60 && marks < 70){
		cout <<"Your grade is C"<<endl;
	}
	else if(marks >= 50 && marks < 60){
		cout <<"Your grade is D"<<endl;
		}
	else{
		cout<<"Your fail this subject"<<endl;
	}
	return 0;
}



