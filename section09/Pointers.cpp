//============================================================================
// Name        : Pointers.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Pointers code in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

int *pointer;
int id = 10;

pointer = &id;
cout << *pointer; // if you add * before pointer you can see integer value
	return 0;
}

