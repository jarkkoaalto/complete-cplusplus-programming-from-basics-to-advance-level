//============================================================================
// Name        : PassValueandPassReference.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Pass by value and pass by reference in C++, Ansi-style
//============================================================================

#include <iostream>

using namespace std;

void changeValue(int* value){
	*value = 50;
}

int main() {

	int number = 10;

	cout<<number<<endl;

	changeValue(&number);

	cout<<number;
	return 0;
}

