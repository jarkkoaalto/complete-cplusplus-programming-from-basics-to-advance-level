//============================================================================
// Name        : Pointers.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Pointers code in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string.h>

using namespace std;

int main() {

	int *intPointer;
	int intVariable = 30;

	float *floatPointer;
	float floatVariable = 20.4;

	floatPointer = &floatVariable;

	string *stringPointer;
	string stringVariable = "Joe";

	stringPointer = &stringVariable;

	cout << floatPointer<<endl;
	cout << stringPointer<<endl;
	return 0;
}

