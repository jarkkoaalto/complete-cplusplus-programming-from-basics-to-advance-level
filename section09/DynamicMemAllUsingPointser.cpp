//============================================================================
// Name        : DynamicMemAllUsingPointser.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Dynamic memory allocation using pointers in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	string *pointerArray;
	int size;
	cout << "How many hobbies you have: " << endl;
	cin>>size;

	pointerArray = new string[size];
	cout <<"Enter your Hobbies: ";

	for(int i=0; i<size; i++){
		cin>>pointerArray[i];
	}

	cout << "Your Hobbies are: " <<endl;
	for(int i=0; i<size; i++){
		cout<<pointerArray[i]<<endl;
	}
	return 0;
}

