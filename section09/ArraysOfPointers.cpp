//============================================================================
// Name        : ArraysOfPointers.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Arrays of Pointers in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string.h>

using namespace std;

int main() {

	int *array[3];

	int a = 3;
	int b = 4;
	int c = 5;

	array[0] = &a;
	array[1] = &b;
	array[2] = &c;

	for (int i=0; i<3; i++){
		// cout<<array[i]<<endl;
		cout<<*array[i]<<endl;
	}

	return 0;
}


